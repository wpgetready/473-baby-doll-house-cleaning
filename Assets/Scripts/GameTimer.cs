﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameTimer : MonoBehaviour {

	public Text txtTimeLeft;
	Animator animTimeLeft;

	bool bTestWatchVideo20Sec = false;
	bool bTestWatchVideo3Sec = false;

	void Start () {
		InvokeRepeating("TimerTick",0f,1f);
		animTimeLeft = txtTimeLeft.transform.GetComponent<Animator>();
	}
	
 
	void TimerTick()
	{
		if(!RoomScene.bPause)
		{
			if(!Tutorial.bTutorial  ) GameData.TimeLeft--;
			if(GameData.TimeLeft <=0)
			{
				GameData.TimeLeft = 0;
				if( !CleaningTool.bCleaning &&  !Item.bCleaning )
				{
					transform.SendMessage("OutOfTime");
					GameData.bOutOfTime = true;
				}
			}
			//if(GameData.TimeLeft >10) txtTimeLeft.color = Color.white;
			//else  txtTimeLeft.color = new Color(1,0.2f,0.2f);
		}

		txtTimeLeft.text = Mathf.FloorToInt(GameData.TimeLeft/60) +":"+ (GameData.TimeLeft%60).ToString().PadLeft(2,'0');
		animTimeLeft.SetBool("bTimerBlink", GameData.TimeLeft <=10 && GameData.TimeLeft >0);

//		if(!GameData.bWatchVideoReady && !bTestWatchVideo20Sec && GameData.TimeLeft <=20)
//		{
//			Debug.Log("test20s");
//			bTestWatchVideo20Sec = true;
//			WebelinxCMS.Instance.IsVideoRewardAvailable(WebelinxCMS.WATCH_VIDEO_ID);
//		}

		//if(!GameData.bWatchVideoReady && 
		if(!bTestWatchVideo3Sec && GameData.TimeLeft <=3)
		{
			Debug.Log("test10s");
			bTestWatchVideo3Sec = true;
			AdsManager.Instance.IsVideoRewardAvailable();
			//brisi test 
			//GameData.bWatchVideoReady = true;
		}


		if(GameData.TimeLeft <=10 && GameData.TimeLeft >0)
		{
			SoundManager.Instance.Play_TimeCountdown();
		}
		else
		{
			SoundManager.Instance.Stop_TimeCountdown();
		}
	}

	public void StopTimer()
	{
		CancelInvoke("TimerTick");
		txtTimeLeft.text = Mathf.FloorToInt(GameData.TimeLeft/60) +":"+ (GameData.TimeLeft%60).ToString().PadLeft(2,'0');
		animTimeLeft.SetBool("bTimerBlink",false);
		SoundManager.Instance.Stop_TimeCountdown();

	}
}
