﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Glow : MonoBehaviour {

	Transform glowRectTransform;
	Image imgGlow;
	public Transform ActiveItem;
	 
	void Start () {
		//glowRectTransform=  transform.GetComponent<RectTransform>();
		imgGlow = transform.GetComponent<Image>();
	}
	
	 
	void LateUpdate () {
		if(ActiveItem != null) 
		{
			imgGlow.enabled = true;
			//glowRectTransform.anchoredPosition = ActiveItem.anchoredPosition;
			transform.position = ActiveItem.transform.position;
		}
		else
		{
			imgGlow.enabled = false;
		}
	}
}
