﻿using UnityEngine;
using System.Collections;

public class GameData : MonoBehaviour {

	public static int CurrentLevel =1;
	public static int LastLevel =1; 

	public static int TimeLeft = 285;
	public static bool bOutOfTime = false;
	public static int VideoBonusTime =  60;
	public static int CompletedTasks = 0;
	public static int TotalTasks = 24;
	public static int HintsLeft = 3;
	public static int VideoHints  = 2;
	public static bool bWatchVideoReady = false;
	public static bool bWatchVideoStart = false;

	public static float AnimationInactiveTime = 3f;

	public static bool TestTutorial = false;

	public int Score = 0;

	public static int TutorialShown = 0;
 
	public static void UnlockLevel()
	{
		if(CurrentLevel == LastLevel && LastLevel <8) LastLevel++;

		if(PlayerPrefs.GetInt("LastLevel",1) < LastLevel)
		{
			PlayerPrefs.SetInt("LastLevel",LastLevel);
		}
	}

	public static void SetNewLevel()
	{
		bOutOfTime = false;
		TimeLeft = 300 - CurrentLevel*15 ;//-260
		 
		HintsLeft = 3;
		VideoHints = 2;

		CompletedTasks = 0;
	}
	
	public static void TuturialOver()
	{
		TutorialShown = 1;
		PlayerPrefs.SetInt("TUTORIAL",1);
	}

	public static void Init()
	{
		bOutOfTime = false;
		TutorialShown = PlayerPrefs.GetInt("TUTORIAL",0);
		if(TutorialShown>0)  Tutorial.bTutorial = false;
		else  Tutorial.bTutorial = true;
		Tutorial.TutorialPhase = 0;
	}


 
}
