﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tutorial : MonoBehaviour {
	public static bool bTutorial = false;
	public Animator animPointer;
	public Button btnHints;

	public GameObject F1Start;
	public GameObject F1End;

	public GameObject F2Start;

	public GameObject F3Start;
	public GameObject F3End;

	public GameObject F4Start;

	public static Tutorial Instance;

	public static int TutorialPhase = 0;
	CleaningTool ct1;
	Item itBear;

	void Start () {
	 
		Instance= this;
		animPointer.gameObject.SetActive(true);


		 
		if(GameData.TutorialShown == 0  || GameData.TestTutorial) 
		{
			if(GameData.TestTutorial) GameData.TutorialShown = 0;

//			Debug.Log("TUTORIAL");
			TutorialPhase = 1;
			StartCoroutine("TutorialProgress");
		}

		btnHints.interactable = false;
	}
	

 

	public void DestroyTut()
	{
		GameObject.Destroy(animPointer.transform.parent.gameObject);
		StopAllCoroutines();
		this.enabled = false;
	}

	IEnumerator TutorialProgress()
	{
	 
		 
		if(TutorialPhase < 3)  //POKAZIVANJE CISCENJA  1.korak
		{
			animPointer.transform.position = F1Start.transform.position;
			animPointer.transform.localScale = new Vector3(-1,1,1);

			yield return new WaitForSeconds(1f);
 
			ct1 = F1Start.transform.FindChild("Tool01Mask").GetChild(0).GetComponent<CleaningTool>()          ;//gameObject.SetActive(false);
			animPointer.SetTrigger("tStart");

			yield return new WaitForSeconds(.5f);
			 
			animPointer.SetTrigger("tDrag");
			animPointer.ResetTrigger("tStart");
			float lerpTime = 0;
			animPointer.transform.localScale = new Vector3(1,1,1);
			ct1.transform.SetParent ( animPointer.transform);
			ct1.transform.localPosition = Vector3.zero;

			while(lerpTime <1.1f)
			{
				lerpTime += Time.deltaTime;
				animPointer.transform.position =  Vector3.Lerp(  F1Start.transform.position,F1End.transform.position,lerpTime);
				yield return new WaitForEndOfFrame();
			}
			animPointer.ResetTrigger("tDrag");
			animPointer.SetTrigger("tRelease");
			yield return new WaitForSeconds(.5f);

			ct1.StartMoveBack();
			TutorialPhase =  2;

			float showAgainTime = 0;
			while(showAgainTime<5)
			{
				showAgainTime += Time.deltaTime;
				yield return new WaitForEndOfFrame();
 
				if( ct1.bDrag)
				{
					showAgainTime = 0;
					yield return new WaitForEndOfFrame();
				}
 
				if(TutorialPhase == 3)
				{
					showAgainTime= 6;
				}
			}
			if(TutorialPhase == 2) TutorialPhase = 1;
			else if(TutorialPhase == 3) yield return new WaitForSeconds(1.5f);
			StartCoroutine("TutorialProgress");
		}


 
		else if( TutorialPhase < 5) //POKAZIVANJE ROTACIJE  - 2. korak
		{
			animPointer.transform.position = F2Start.transform.position;
			yield return new WaitForSeconds(2f);
			animPointer.SetTrigger("tStart");

			TutorialPhase = 4;
			while(TutorialPhase == 4)
			{
				yield return new WaitForEndOfFrame();
			}
			animPointer.ResetTrigger("tStart");
			animPointer.SetTrigger("tFadeOut");
			yield return new WaitForSeconds(1f);
			animPointer.ResetTrigger("tFadeOut");
			 StartCoroutine("TutorialProgress");
			TutorialPhase = 6;
		}


		//***********************************************************




		else if( TutorialPhase < 8) //POKAZIVANJE POMERANJA
		{
			 

			animPointer.transform.position = F3Start.transform.position;
		 
			
			yield return new WaitForSeconds(2);
			
			animPointer.SetTrigger("tStart");
			F3Start.transform.GetChild(0).GetComponent<Renderer>().sortingOrder = -1;
			F3Start.transform.GetChild(0).GetComponent<Renderer>().sortingLayerName = "UI";
 

			yield return new WaitForSeconds(2);

			animPointer.SetTrigger("tDrag");
			animPointer.ResetTrigger("tStart");
			float lerpTime = 0;
		 
			while(lerpTime <1.1f)
			{
				lerpTime += Time.deltaTime;
				animPointer.transform.position =  Vector3.Lerp(  F3Start.transform.position,F3End.transform.position,lerpTime);
				F3Start.transform.GetChild(0).position = animPointer.transform.position;
				yield return new WaitForEndOfFrame();
			}
			animPointer.ResetTrigger("tDrag");
			animPointer.SetTrigger("tRelease");

			Vector3 movedPos = F3Start.transform.GetChild(0).localPosition;
			lerpTime = 0;
			yield return new WaitForSeconds(.5f);
			while(lerpTime <1.1f)
			{
				lerpTime += Time.deltaTime*2;
				F3Start.transform.GetChild(0).localPosition  =  Vector3.Lerp( movedPos, Vector3.zero, lerpTime);
				yield return new WaitForEndOfFrame();
			}
			animPointer.ResetTrigger("tRelease");
			 
		//	if(itBear == null ) itBear =  F3Start.transform.GetChild(0).GetComponent<Item>();

			yield return new WaitForSeconds(.5f);

			TutorialPhase =  7;
			
			float showAgainTime = 0;
			while(showAgainTime<5)
			{
				showAgainTime += Time.deltaTime;
				yield return new WaitForEndOfFrame();
				
				if( ct1.bDrag)
				{
					showAgainTime = 0;
					yield return new WaitForEndOfFrame();
				}
				
				if(TutorialPhase == 8)
				{
					showAgainTime= 6;
				}
			}

			if(TutorialPhase == 7) TutorialPhase = 6;
			else if(TutorialPhase == 8) yield return new WaitForSeconds(1.5f);
			StartCoroutine("TutorialProgress");


		}







		//************************************************************
		else if( TutorialPhase < 10) //POKAZIVANJE SAVETA
		{
			 
			animPointer.transform.position = F4Start.transform.position;
			animPointer.transform.localScale = new Vector3(-1,1,1);


 
			yield return new WaitForSeconds(2f);
			animPointer.SetTrigger("tStart");

			btnHints.interactable = true;
			
			TutorialPhase = 9;
			while(TutorialPhase == 9)
			{
				yield return new WaitForEndOfFrame();
			}
			animPointer.ResetTrigger("tStart");
			animPointer.SetTrigger("tFadeOut");
			TutorialPhase = 10;
			yield return new WaitForSeconds(1f);
			animPointer.ResetTrigger("tFadeOut");

		}



	}

	/*
	IEnumerator Test()
	{
		yield return new WaitForSeconds(3);
		animPointer.SetTrigger("tStart");
		yield return new WaitForSeconds(3);
		animPointer.SetTrigger("tDrag");

		yield return new WaitForSeconds(2);
		animPointer.SetTrigger("tRelease");

		yield return new WaitForSeconds(1);
		animPointer.SetTrigger("tStart");
		yield return new WaitForSeconds(3);
		animPointer.SetTrigger("tDrag");
		
		yield return new WaitForSeconds(1);
		animPointer.SetTrigger("tFadeOut");

		yield return new WaitForSeconds(1);
		animPointer.SetTrigger("tStart");
		yield return new WaitForSeconds(3);
		animPointer.SetTrigger("tDrag");
	}
	*/
}
