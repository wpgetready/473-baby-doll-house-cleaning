﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MapScene : MonoBehaviour {

	public Sprite SoundOn;
	public Sprite SoundOff;
	public Image BtnSoundIcon;
	
	public GameObject PopUpCrossPromotion;
	public GameObject PopUpAreYouSure;
	public MenuManager menuManager;
	CanvasGroup BlockAll;

	void Start () {
		BlockAll = GameObject.Find("Canvas/BlockAll").transform.GetComponent<CanvasGroup>();
		BlockAll.blocksRaycasts = false;

		if(SoundManager.soundOn == 0) 
			BtnSoundIcon.sprite = SoundOff;
		else  
			BtnSoundIcon.sprite = SoundOn;

		for(int i = 1; i<=8;i++)
		{
			GameObject.Find("ButtonPlayRoom"+i.ToString()).transform.GetComponent<Button>().interactable = (i<=GameData.LastLevel);

		}
	}
	 

	public void btnSoundClick()
	{
		if(SoundManager.soundOn == 0)
		{
			SoundManager.Instance.SetSound(true);
			BtnSoundIcon.sprite = SoundOn;
		}
		else 
		{
			SoundManager.Instance.SetSound(false);
			BtnSoundIcon.sprite = SoundOff;
		}
		SoundManager.Instance.Play_ButtonClick();
	}
	
	public void btnPlayClick(int roomNo)
	{
		SoundManager.Instance.Play_ButtonClick();
		//roomNo = 8;
		GameData.CurrentLevel =roomNo;
		RoomScene.selectedRoom = roomNo;
		StartCoroutine(PlayRoom());
		BlockAll.blocksRaycasts = true;
	}

	 IEnumerator PlayRoom()
	{
		yield return new WaitForSeconds(.3f);
		Application.LoadLevel("Room");
		
	} 
	
	public void btnMoreGamesClick()
	{
		SoundManager.Instance.Play_ButtonClick();
		menuManager.ShowPopUpMenu(PopUpCrossPromotion);
	}
	
	public void btnBackClick()
	{
		SoundManager.Instance.Play_ButtonClick();
		menuManager.ShowPopUpMenu(PopUpAreYouSure);
	}

	public void btnBackNo()
	{
		SoundManager.Instance.Play_ButtonClick();
		menuManager.ClosePopUpMenu(PopUpAreYouSure);
	}

	public void btnBackYes()
	{
		SoundManager.Instance.Play_ButtonClick();
		GameData.LastLevel =1;


		BlockAll.blocksRaycasts = true;
		menuManager.ClosePopUpMenu(PopUpAreYouSure);
		StartCoroutine(Back());
	}

	IEnumerator Back()
	{
		yield return new WaitForSeconds(.8f);
		Application.LoadLevel("Home");
		
	}

	IEnumerator SetBlockAll(float time, bool blockRays)
	{
		if(BlockAll == null) BlockAll = GameObject.Find("Canvas/BlockAll").GetComponent<CanvasGroup>();
		yield return new WaitForSeconds(time);
		BlockAll.blocksRaycasts = blockRays;
		BlockAll.alpha = blockRays?1:0;
	}

	public void ClickSound()
	{
		SoundManager.Instance.Play_ButtonClick();
	}
}
