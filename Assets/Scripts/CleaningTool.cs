﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using UnityEngine.EventSystems;

public class CleaningTool : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

	public ToolType toolType;
	Vector3 StartPosition ;
	bool bIskoriscen = false;
	public  bool bDrag = false;

	Transform StartParent;
	Transform ActiveToolParent;
	RectTransform recTransform;
	float x;
	float y;
	Vector3 diffPos = new Vector3(0,0,0);
	public int ToolNo = 0;

	public static int activeToolNo = 0;
	public static bool bCleaning = false;

	public static ToolType[] ReturnRoomTools(int room)
	{
		ToolType[] tools  = new ToolType[3];
		tools[0] = ToolType.none;
		tools[1] = ToolType.none;
		tools[2] = ToolType.none;

		if(room == 1)
		{
			tools[0] = ToolType.duster;
			tools[1] = ToolType.brush;
			tools[2] = ToolType.sponge;
		}

		if(room == 2)
		{
			tools[0] = ToolType.duster;
			tools[1] = ToolType.brush;
			tools[2] = ToolType.sponge;
		}

		if(room == 3)
		{
			tools[0] = ToolType.duster;
			tools[1] = ToolType.brush;
			tools[2] = ToolType.sponge;
		}


		if(room == 4)
		{
			tools[0] = ToolType.duster;
			tools[1] = ToolType.brush;
			tools[2] = ToolType.sponge;
		}



		if(room == 5)
		{
			tools[0] = ToolType.duster;
			tools[1] = ToolType.brush;
			tools[2] = ToolType.sponge;
		}

		if(room == 6)
		{
			tools[0] = ToolType.duster;
			tools[1] = ToolType.brush;
			tools[2] = ToolType.sponge;
		}

		if(room == 7)
		{
			tools[0] = ToolType.duster;
			tools[1] = ToolType.brush;
			tools[2] = ToolType.sponge;
		}

		if(room == 8)
		{
			tools[0] = ToolType.duster;
			tools[1] = ToolType.brush;
			tools[2] = ToolType.sponge;
		}

		return tools;
	}

	public void Start()
	{
		bCleaning = false;
		bDrag = false;
		activeToolNo = 0;

		recTransform = transform.GetComponent<RectTransform>();
		switch(toolType) 
		{
		case ToolType.sponge:
			recTransform.anchoredPosition = Vector3.zero;
			break;
		case ToolType.duster:
			recTransform.anchoredPosition = Vector3.zero;
			break;
		case ToolType.brush:
			recTransform.anchoredPosition = Vector3.zero;
			break;
		case ToolType.broom:
			recTransform.anchoredPosition = new Vector3(48,69,0);
			break;

		}
		StartPosition  = transform.position;
		StartParent = transform.parent;
		ActiveToolParent = GameObject.Find("ActiveTool").transform;
//		Debug.Log(transform.name + ",   "+ transform.parent.name +",  "+StartPosition);
	}
	

	void Update()
	{
		 
		if(GameData.bOutOfTime && bDrag)
		{
			StartCoroutine("MoveBack");
			bDrag = false;
		 
		}
	}
	
	public static GameObject  itemBeingDragged;
	
	#region IBeginDragHandler implementation
	
	public void OnBeginDrag (PointerEventData eventData)
	{
		if(!GameData.bOutOfTime)
		{
			bCleaning = false;
			if(Tutorial.bTutorial && GameData.TutorialShown == 0)
			{
				if(Tutorial.TutorialPhase != 2 || ToolNo!=1)
				{
					return;
				}
			}

			if(  !bIskoriscen   && !bDrag  && activeToolNo == 0 )
			{
				SoundManager.Instance.Play_ToolClick();
				bDrag = true;
				diffPos =transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition)   ;
				diffPos = new Vector3(diffPos.x,diffPos.y,0);

				transform.parent = ActiveToolParent;

				activeToolNo = ToolNo;
			}
		}
 
	}
	
	#endregion
	
	#region IDragHandler implementation
	
	public void OnDrag (PointerEventData eventData)
	{

		if(  !bIskoriscen &&  bDrag  && activeToolNo == ToolNo)
		{
			x = Input.mousePosition.x;
			y = Input.mousePosition.y;
			transform.position = Camera.main.ScreenToWorldPoint(new Vector3(x ,y,100.0f)) + diffPos; //100 - canvas plane distance
		}

		
	}
	
	#endregion
	
	#region IEndDragHandler implementation
	
	public void OnEndDrag (PointerEventData eventData)
	{
		if(!GameData.bOutOfTime)
		{

			if(  !bIskoriscen &&  bDrag && activeToolNo == ToolNo  )
			{
				x = Input.mousePosition.x;
				y = Input.mousePosition.y;
				transform.position = Camera.main.ScreenToWorldPoint(new Vector3(x ,y,100.0f)) + diffPos;
				
				transform.localScale = Vector3.one;

				bDrag = false;
				 
				Collider2D[] hitColliders = Physics2D.OverlapCircleAll(transform.position, .5f  , 1 << LayerMask.NameToLayer("Tool"+ToolNo.ToString()+"Interact")); //layermask to filter the varius colliders
				if(hitColliders.Length > 0 )
				{
					Transform trPom = null;
					for (int i =0 ; i<hitColliders.Length; i++)    
					{
						//TODO: pronaci najblizi
	//					Debug.Log(hitColliders[i].name);
						 
						hitColliders[i].transform.SendMessage("FadeOut_CleaningTool");
						RoomScene.ToolActionsLeft[ToolNo-1] --;
						trPom = hitColliders[i].transform;
						if(RoomScene.ToolActionsLeft[ToolNo-1] <= 0) bIskoriscen = true;
					}

					if( toolType == ToolType.sponge  ) transform.GetChild(0).GetComponent<Animator>().SetTrigger("tUpDownClean");
					if( toolType == ToolType.brush ) transform.GetChild(0).GetComponent<Animator>().SetTrigger("tLeftRightClean");
					if( toolType == ToolType.duster  ) transform.GetChild(0).GetComponent<Animator>().SetTrigger("tDusterClean");

					Vector3 poz2 = trPom.position;

					if( toolType == ToolType.sponge  ) poz2 = trPom.position;
					if( toolType == ToolType.brush ) poz2 = trPom.position+ new Vector3(0,0.3f,0);
					if( toolType == ToolType.duster  ) poz2 = trPom.position+ new Vector3(-.2f,0f,0);

					StartCoroutine("CleanAndMoveBack",poz2 );


					if(Tutorial.bTutorial && GameData.TutorialShown == 0)
					{
						if(Tutorial.TutorialPhase == 2) 
						{
							Tutorial.TutorialPhase = 3;
						}
					}
				}
				else
				{
					SoundManager.Instance.Play_Error();
					StartCoroutine("MoveBack" );

				}
			}

		}
		else
		{
			//NEMA VREMANA
			StartCoroutine("MoveBack" );
		}
	}
		#endregion


	IEnumerator CleanAndMoveBack( Vector3 poz2  )
	{


		float pom = 0;
		RoomScene.bTaskAnim = true;
		bCleaning = true;

		while(pom<1 ) 
		{ 
			pom +=0.02f*10;
			transform.position = Vector3.Lerp(transform.position, poz2, pom);
			yield return new WaitForSeconds(0.02f);
		}

		yield return new WaitForSeconds(0.1f);
		yield return new WaitForEndOfFrame( );
		float timeWait = transform.GetChild(0).GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length;
		yield return new WaitForSeconds(timeWait);
		transform.parent = StartParent;

		transform.parent.GetComponent<Mask>().enabled =false;
		transform.parent.GetComponent<Image>().enabled =false;
 
		yield return new WaitForEndOfFrame( );
		 pom = 0;
		while(pom<1 ) 
		{ 
			pom +=0.02f*5;
			transform.position = Vector3.Lerp(transform.position, StartPosition,pom);
			yield return new WaitForSeconds(0.02f);
		}
		 
		transform.position = StartPosition;
	 
		transform.parent.GetComponent<Mask>().enabled =true;
		transform.parent.GetComponent<Image>().enabled =true;

	 	activeToolNo = 0;
		if(bIskoriscen)
		{
			//	transform.GetChild(0).GetComponent<Image>().color = new Color(1,1,1,0.5f);
			transform.parent.parent.FindChild("toolInactive").GetComponent<Image>().enabled = true;  //color = new Color(1,1,1,1);
		}

		yield return new WaitForSeconds(.5f);
		bCleaning = false;
	}

	bool bMovingBack = false;
	IEnumerator MoveBack(  )
	{

		yield return new WaitForEndOfFrame( );
		if(!bMovingBack)
		{
			bMovingBack = true;
			transform.parent = StartParent;

			transform.parent.GetComponent<Mask>().enabled =false;
			transform.parent.GetComponent<Image>().enabled =false;
			
			yield return new WaitForEndOfFrame( );
			float pom = 0;
			while(pom<1 )
			{ 
				pom +=0.02f*5;
				transform.position = Vector3.Lerp(transform.position, StartPosition,pom);
				yield return new WaitForSeconds(0.02f);
			}
		 
				transform.position = StartPosition;

			transform.parent.GetComponent<Mask>().enabled =true;
			transform.parent.GetComponent<Image>().enabled =true;
			activeToolNo = 0;
			bMovingBack = false;
		}
		 
	}

	public void StartMoveBack()
	{
		StartCoroutine("MoveBack" );
	}
	
}

public enum ToolType
{
	mop,
	cloth,
	glass,
	duster,
	sponge,
	broom,
	brush,
	none

}