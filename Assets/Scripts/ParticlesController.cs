﻿using UnityEngine;
using System.Collections;

public class ParticlesController : MonoBehaviour {

	public ParticleSystem PS1;
	public ParticleSystem PS2;
	public ParticleSystem PS3;


	bool bPS1Active = false;
	bool bPS2Active = false;
	bool bPS3Active = false;

	Vector3 ps1Start, ps1End ;
	Vector3 ps2Start, ps2End ;
	Vector3 ps3Start, ps3End ;



	// Use this for initialization
	void Start () 
	{
		PS1.GetComponent<Renderer>().sortingLayerName = "TOOL";
		PS2.GetComponent<Renderer>().sortingLayerName = "TOOL";
		PS3.GetComponent<Renderer>().sortingLayerName = "TOOL";

		for(int i =0; i < PS1.transform.childCount;i++)
		{
			PS1.transform.GetChild(i).GetComponent<Renderer>().sortingLayerName = "TOOL";
			PS2.transform.GetChild(i).GetComponent<Renderer>().sortingLayerName = "TOOL";
			PS3.transform.GetChild(i).GetComponent<Renderer>().sortingLayerName = "TOOL";
		}

		PS1.Stop ();
		PS2.Stop ();
		PS3.Stop ();
	}

 
	public void ShootPS(Vector3 startPos, Vector3 endPos)
	{
		startPos = new Vector3(startPos.x,startPos.y,-1);
		endPos = new Vector3(endPos.x,endPos.y,-1);
		if(!bPS1Active) StartCoroutine(MovePS1(startPos,endPos));
		else if(!bPS2Active) StartCoroutine(MovePS2(startPos,endPos));
		else if(!bPS3Active) StartCoroutine(MovePS3(startPos,endPos));
	}

	IEnumerator MovePS1(Vector3 startPos, Vector3 endPos)
	{
		bPS1Active = true;
		PS1.transform.position = startPos;
		yield return new WaitForEndOfFrame();
		PS1.Play ();
		float timeM = 0;
		while(timeM <1)
		{
			PS1.transform.position = Vector3.Lerp(startPos,endPos,timeM);
			timeM += (Time.deltaTime*2);//*.5f
			yield return new WaitForEndOfFrame();
		}
		PS1.transform.position = endPos;
		PS1.Stop ();
		bPS1Active = false;
		 

		if( CleaningTool.bCleaning) yield return new WaitForSeconds(.7f);
		else yield return new WaitForSeconds(.3f);
		RoomScene.bTaskAnim =   bPS1Active || bPS2Active || bPS3Active || CleaningTool.bCleaning || Item.bCleaning; 
	}

	IEnumerator MovePS2(Vector3 startPos, Vector3 endPos)
	{
		bPS2Active = true;
		yield return new WaitForEndOfFrame();
		PS2.Play ();
		float timeM = 0;
		while(timeM <1)
		{
			PS2.transform.position = Vector3.Lerp(startPos,endPos,timeM);
			timeM += (Time.deltaTime*2);
			yield return new WaitForEndOfFrame();
		}
		PS2.transform.position = endPos;
		PS2.Stop ();
		bPS2Active = false;
	
		if( CleaningTool.bCleaning) yield return new WaitForSeconds(.7f);
		else yield return new WaitForSeconds(.3f);
		RoomScene.bTaskAnim =   bPS1Active || bPS2Active || bPS3Active || CleaningTool.bCleaning || Item.bCleaning; 
	}

	IEnumerator MovePS3(Vector3 startPos, Vector3 endPos)
	{
		bPS3Active = true;
		yield return new WaitForEndOfFrame();
		PS3.Play ();
		float timeM = 0;
		while(timeM <1)
		{
			PS3.transform.position = Vector3.Lerp(startPos,endPos,timeM);
			timeM += (Time.deltaTime*2);
			yield return new WaitForEndOfFrame();
		}
		PS3.transform.position = endPos;
		PS3.Stop ();
		bPS3Active = false;

		if( CleaningTool.bCleaning) yield return new WaitForSeconds(.7f);
		else yield return new WaitForSeconds(.3f);
		RoomScene.bTaskAnim =   bPS1Active || bPS2Active || bPS3Active || CleaningTool.bCleaning || Item.bCleaning; 
	}
}
