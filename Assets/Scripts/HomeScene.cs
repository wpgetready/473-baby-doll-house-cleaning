﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class HomeScene : MonoBehaviour {

	public Sprite SoundOn;
	public Sprite SoundOff;
	public Image BtnSoundIcon;

	public GameObject PopUpCrossPromotion;
	public GameObject PopUpRate;
	public GameObject PopUpInterstitial;
	public GameObject PopUpHelp;

	public MenuManager menuManager;
	CanvasGroup BlockAll;
 

	void Start () {
		Input.multiTouchEnabled = false;

		//menuManager.ShowPopUpMenu(PopUpInterstitial);
		BlockAll = GameObject.Find("Canvas/BlockAll").transform.GetComponent<CanvasGroup>();
		BlockAll.blocksRaycasts = false;

		if(SoundManager.soundOn == 0) 
			BtnSoundIcon.sprite = SoundOff;
		else  
			BtnSoundIcon.sprite = SoundOn;
 

		GameData.Init();
	}

	
	public void ExitGame () {
 
		AdsManager.Instance.ShowInterstitial();
		
	}

	 
	public void btnSoundClick()
	{

		if(SoundManager.soundOn == 0)
		{
			SoundManager.Instance.SetSound(true);
			BtnSoundIcon.sprite = SoundOn;
		}
		else 
		{
			SoundManager.Instance.SetSound(false);
			BtnSoundIcon.sprite = SoundOff;
		}
		SoundManager.Instance.Play_ButtonClick();
	}

	public void btnPlayClick()
	{
		SoundManager.Instance.Play_ButtonClick();
		StartCoroutine(LoadMap());
	}
	
	IEnumerator LoadMap()
	{
		yield return new WaitForSeconds(.3f);
		Application.LoadLevel("Map");
		
	}

	public void btnMoreGamesClick()
	{
		SoundManager.Instance.Play_ButtonClick();
		menuManager.ShowPopUpMenu(PopUpCrossPromotion);
	}

	public void btnHelpClick()
	{
		SoundManager.Instance.Play_ButtonClick();
		menuManager.ShowPopUpMenu(PopUpHelp);
	}

	public void btnCloseHelpClick()
	{
		SoundManager.Instance.Play_ButtonClick();
		menuManager.ClosePopUpMenu(PopUpHelp);
	}

	IEnumerator SetBlockAll(float time, bool blockRays)
	{
		if(BlockAll == null) BlockAll = GameObject.Find("ForegroundBlockAll").GetComponent<CanvasGroup>();
		yield return new WaitForSeconds(time);
		BlockAll.blocksRaycasts = blockRays;
		BlockAll.alpha = blockRays?1:0;
	}


	public void ClickSound()
	{
		SoundManager.Instance.Play_ButtonClick();
	}

 
}
