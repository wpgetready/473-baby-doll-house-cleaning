﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class EscapeButtonManager : MonoBehaviour {

	bool bDisableEsc = false;
	public static  Stack<string> EscapeButonFunctionStack = new Stack<string>();

	void Start () {
		DontDestroyOnLoad (this.gameObject);
	
	}

	void OnLevelWasLoaded(int level) {

		EscapeButonFunctionStack.Clear();
		bDisableEsc = false;
		if(Application.loadedLevelName == "Home") AddEscapeButonFunction("ExitGame");
		if(Application.loadedLevelName == "Map") AddEscapeButonFunction("btnBackClick");
		if(Application.loadedLevelName == "Room") AddEscapeButonFunction("btnPauseClick");
	}
	 
	public static void  AddEscapeButonFunction( string functionName, string functionParam = "")
	{
		if(functionParam != "") functionName +="*"+functionParam;
		EscapeButonFunctionStack.Push(functionName);
	}




	void Update()
	{
		//if(  EscapeButonFunctionStack.Count > 0 ) Debug.Log( EscapeButonFunctionStack.Count + "    " + EscapeButonFunctionStack.Peek() );
		
		if( !bDisableEsc  && Input.GetKeyDown(KeyCode.Escape) )
		{
			if(  EscapeButonFunctionStack.Count > 0 )  Debug.Log( EscapeButonFunctionStack.Count + "    " + EscapeButonFunctionStack.Peek() );
			
			if(EscapeButonFunctionStack.Count>0)
			{
				bDisableEsc = true;

				if( EscapeButonFunctionStack.Peek().Contains("*") )
				{
					
					string[] funcAndParam = EscapeButonFunctionStack.Peek().Split('*');
					Debug.Log(funcAndParam[0]);
					if(funcAndParam[0] == "ClosePopUpMenuEsc") 
					{
					/*	if (Application.loadedLevelName == "Home")
						{
							Debug.Log(funcAndParam[1]);
							if (funcAndParam[1] == "PopUpHelp")
							{
								MenuManager go = GameObject.Find("Canvas").GetComponent<MenuManager>();
								go.helpNativeAd.GetComponent<FacebookNativeAd>().CancelLoading();
								go.helpNativeAd.GetComponent<FacebookNativeAd>().HideNativeAd();
								go.mainSceneNativeAd.GetComponent<FacebookNativeAd>().LoadAd();
							}
							else if (funcAndParam[1] == "PopUpRate" || funcAndParam[1] == "PopUpCrossPromotionOfferWall")
							{
								MenuManager go = GameObject.Find("Canvas").GetComponent<MenuManager>();
								go.mainSceneNativeAd.GetComponent<FacebookNativeAd>().LoadAd();
							}
						}

						if (Application.loadedLevelName == "Map")
						{
							if (funcAndParam[1] == "PopUpCrossPromotionOfferWall" || funcAndParam[1] == "PopAreYouSure")
							{
								MenuManager go = GameObject.Find("Canvas").GetComponent<MenuManager>();
								go.mainSceneNativeAd.GetComponent<FacebookNativeAd>().LoadAd();
							}
						}
*/
						GameObject.Find("Canvas").SendMessage(funcAndParam[0],funcAndParam[1], SendMessageOptions.DontRequireReceiver);
					}
					else
					{
						Camera.main.SendMessage(funcAndParam[0],funcAndParam[1], SendMessageOptions.DontRequireReceiver);
						EscapeButonFunctionStack.Pop();
					}
				}
				else
				{
					if( EscapeButonFunctionStack.Count == 1 && EscapeButonFunctionStack.Peek() == "btnPauseClick" ) 
						Camera.main.SendMessage("btnPauseClick", SendMessageOptions.DontRequireReceiver); //pauza se ne uklanja iz staka ako je na prvom mestu
					else 
						Camera.main.SendMessage(EscapeButonFunctionStack.Pop(), SendMessageOptions.DontRequireReceiver);
				}
			} 
			StartCoroutine("DisableEsc");
		}
	}
	
	IEnumerator DisableEsc()
	{
		yield return new WaitForSeconds(2);
		bDisableEsc = false;
	}


}



