﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemsData 
{
	public static Dictionary <string,ItemData> Room1 = new Dictionary <string,ItemData>();
	public static Dictionary <string,ItemData> Room2 = new Dictionary <string,ItemData>();
	public static Dictionary <string,ItemData> Room3 = new Dictionary <string,ItemData>();
	public static Dictionary <string,ItemData> Room4 = new Dictionary <string,ItemData>();
	public static Dictionary <string,ItemData> Room5 = new Dictionary <string,ItemData>();
	public static Dictionary <string,ItemData> Room6 = new Dictionary <string,ItemData>();
	public static Dictionary <string,ItemData> Room7 = new Dictionary <string,ItemData>();
	public static Dictionary <string,ItemData> Room8 = new Dictionary <string,ItemData>();

	public static Dictionary <string,ItemData> ActiveRoom = new Dictionary <string,ItemData>();
	 
	public static void Init(int room)
	{
		if(room == 1 )
		{
			if( Room1.Count ==0)
			{
				Room1.Add( "Cobweb*1", new ItemData( ){ ACTION= ItemActionType.clean, START_POS_ANCHORS = "A1;A3", SCALE = new Vector2(40,40), START_ROT = -45, TOOL =  ToolType.duster});
				Room1.Add( "Cobweb*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A2", SCALE = new Vector2(30,30)  ,START_ROT = 0, TOOL = ToolType.duster});
				Room1.Add( "Stain*1", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A4;A5",SCALE = new Vector2(35,35) , START_ROT = -170, TOOL = ToolType.sponge});
				Room1.Add( "Stain*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A6",SCALE = new Vector2(30,30) , START_ROT = 28, TOOL = ToolType.sponge}); 
				Room1.Add( "Dirt*1", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A7;A9",SCALE = new Vector2(40,30) , START_ROT = 0, TOOL = ToolType.brush});
				Room1.Add( "Dirt*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A8",SCALE = new Vector2(40,30) , START_ROT = 0, TOOL = ToolType.brush});

				Room1.Add( "Pillow_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E2", START_POS_ANCHORS = "A18;A34" ,SCALE = new Vector2(35,35), START_ROT = 0, END_ROT = 0.01f});
				Room1.Add( "Knob_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E5", START_POS_ANCHORS = "A21;A30" ,SCALE = new Vector2(35,35) , START_ROT = 0, END_ROT = 0.01f});

				Room1.Add( "Blanket_A", new ItemData( ){ ACTION= ItemActionType.changeSprite,  END_POS_ANCHOR = "E7", START_POS_ANCHORS = "A23" ,SCALE = new Vector2(35,35)  , START_ROT = 0, END_ROT = 0.01f});
				Room1.Add( "LampShade_A", new ItemData( ){ ACTION= ItemActionType.changeSprite,  END_POS_ANCHOR = "E9", START_POS_ANCHORS = "A25" ,SCALE = new Vector2(35,35)  , START_ROT = 0, END_ROT =-90});

				Room1.Add( "Drawer", new ItemData( ){ ACTION= ItemActionType.eraseSprite, START_POS_ANCHORS = "A11", SCALE = new Vector2(37,35) , ITEMS_LIST = "Benkica;Sweater;Sock2;Sock1;Shorts", START_ROT = 90, END_ROT =90 });


				Room1.Add( "Candle", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E8",  SCALE = new Vector2(35,35), START_POS_ANCHORS = "A24" , START_ROT = 0, END_ROT = 0.01f});
				Room1.Add( "Sweater", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E1", START_POS_ANCHORS = "A17;A32"  , SCALE = new Vector2(35,35) , ITEM_HOLDER = "Drawer", DESTROY_WITH_I_H = true, END_SCALE_FACTOR = .6f });
				Room1.Add( "Benkica", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E4", START_POS_ANCHORS = "A20", SCALE = new Vector2(39,39) , ITEM_HOLDER = "Drawer", DESTROY_WITH_I_H = true, END_SCALE_FACTOR = .7f });
				Room1.Add( "Sock2", new ItemData( ){ ACTION= ItemActionType.move,   START_POS_ANCHORS = "A26;A35" ,SCALE = new Vector2(35,35)   ,  END_POS_ANCHOR = "E10",ITEM_HOLDER = "Drawer", DESTROY_WITH_I_H = true, END_SCALE_FACTOR = .7f });
				Room1.Add( "Sock1", new ItemData( ){ ACTION= ItemActionType.move,   START_POS_ANCHORS = "A27;A28;A33"  ,  END_POS_ANCHOR = "E11", ITEM_HOLDER = "Drawer", DESTROY_WITH_I_H = true, END_SCALE_FACTOR = .7f });
				Room1.Add( "Shorts", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E3", START_POS_ANCHORS = "A19;A31" , SCALE = new Vector2(35,35) , ITEM_HOLDER = "Drawer", DESTROY_WITH_I_H = true, END_SCALE_FACTOR = .7f });


				Room1.Add( "Picture1", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A15", START_ROT = -25, END_ROT = 0.01f , SCALE = new Vector2(45,45) });
				Room1.Add( "Picture2", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A16", START_ROT = 15, END_ROT =  0.01f , SCALE = new Vector2(45,45)});
				Room1.Add( "Picture3", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A14",  START_ROT = -15, END_ROT = 0.01f , SCALE = new Vector2(45,45)});


				Room1.Add( "Rocket", new ItemData( ){ ACTION= ItemActionType.move,   START_POS_ANCHORS = "A29;A36",  SCALE = new Vector2(35,35), END_POS_ANCHOR = "E12" , START_ROT = 90, END_ROT = 90f});
				Room1.Add( "PalmTree", new ItemData( ){ ACTION= ItemActionType.move,   START_POS_ANCHORS = "A10", END_POS_ANCHOR = "E6", SCALE = new Vector2(45,45) });
				Room1.Add( "BabyMonitor", new ItemData( ){ ACTION= ItemActionType.move,   START_POS_ANCHORS = "A12" , END_POS_ANCHOR = "E14",SCALE = new Vector2(35,35) });
				Room1.Add( "PictureFrame", new ItemData( ){ ACTION= ItemActionType.move,  SCALE = new Vector2(35,35), END_POS_ANCHOR = "E15",   START_POS_ANCHORS = "A13", START_ROT = 0, END_ROT =  0.01f  });


			}
			
			ActiveRoom = Room1;
			//25
		}


		if(room == 2 )
		{
			if( Room2.Count ==0)
			{

				Room2.Add( "Cobweb*1", new ItemData( ){ ACTION= ItemActionType.clean, START_POS_ANCHORS = "A1;A2", SCALE = new Vector2(35,35), START_ROT = 0, TOOL =  ToolType.duster});
				Room2.Add( "Cobweb*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A3;A4", SCALE = new Vector2(35,35)  ,START_ROT = 0, TOOL = ToolType.duster});
				Room2.Add( "Stain*1", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A5",SCALE = new Vector2(45,45) , START_ROT = 0, TOOL = ToolType.sponge});
				Room2.Add( "Stain*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A6;A7",SCALE = new Vector2(45,45) , START_ROT =0, TOOL = ToolType.sponge}); 
				Room2.Add( "Dirt*1", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A8",SCALE = new Vector2(35,35) , START_ROT = 0, TOOL = ToolType.brush});
				Room2.Add( "Dirt*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A9",SCALE = new Vector2(35,35) , START_ROT = 0, TOOL = ToolType.brush});


				Room2.Add( "Door", new ItemData( ){ ACTION= ItemActionType.eraseSprite,   START_POS_ANCHORS = "A10",SCALE = new Vector2(36f,35f), ITEMS_LIST = "HairCurlers;HairBrushRound;HairGel;HairDryer;HairBrushFlat"});

 
				Room2.Add( "HairCurlers", new ItemData( ){ ACTION= ItemActionType.move,   START_POS_ANCHORS = "A22;A36" , SCALE = new Vector2(36,36)    ,END_POS_ANCHOR = "E12", END_SCALE_FACTOR = 0.001f , ITEM_HOLDER = "Door", DESTROY_WITH_I_H = true});
				Room2.Add( "HairBrushRound", new ItemData( ){ ACTION= ItemActionType.move,   START_POS_ANCHORS = "A23;A35" , SCALE = new Vector2(35,35)  ,END_POS_ANCHOR = "E13", END_SCALE_FACTOR = 0.001f  , ITEM_HOLDER = "Door", DESTROY_WITH_I_H = true});
				Room2.Add( "HairGel", new ItemData( ){ ACTION= ItemActionType.move,   START_POS_ANCHORS = "A25", SCALE = new Vector2(36,36) , START_ROT =  90  ,END_POS_ANCHOR = "E15", END_SCALE_FACTOR = 0.001f , ITEM_HOLDER = "Door", DESTROY_WITH_I_H = true });
				Room2.Add( "HairDryer", new ItemData( ){ ACTION= ItemActionType.move,   START_POS_ANCHORS = "A26;A37" , SCALE = new Vector2(30,30) ,END_POS_ANCHOR = "E16", END_SCALE_FACTOR = 0.001f , ITEM_HOLDER = "Door", DESTROY_WITH_I_H = true} );
				Room2.Add( "HairBrushFlat", new ItemData( ){ ACTION= ItemActionType.move,   START_POS_ANCHORS = "A27" , SCALE = new Vector2(36,36), START_ROT =  90  ,END_POS_ANCHOR = "E17", END_SCALE_FACTOR = 0.001f , ITEM_HOLDER = "Door", DESTROY_WITH_I_H = true} );


				Room2.Add( "DoorWashingMachine_A", new ItemData( ) { ACTION= ItemActionType.changeSprite,  END_POS_ANCHOR = "E11", START_POS_ANCHORS = "A34", SCALE = new Vector2(40,40) , START_ROT =  0, END_ROT = 90 , 
					ITEMS_LIST = "Shorts;Sweater;Benkica;Sock1;Sock2" });

				Room2.Add( "Shorts", new ItemData( ){ ACTION= ItemActionType.move,   START_POS_ANCHORS = "A11" , SCALE = new Vector2(36,36) , END_POS_ANCHOR = "E1", END_SCALE_FACTOR = 0.701f, ITEM_HOLDER = "DoorWashingMachine_A", DESTROY_WITH_I_H = false});
				Room2.Add( "Sweater", new ItemData( ){ ACTION= ItemActionType.move,   START_POS_ANCHORS = "A12", SCALE = new Vector2(36f,36f), END_POS_ANCHOR = "E2", END_SCALE_FACTOR = 0.701f, ITEM_HOLDER = "DoorWashingMachine_A", DESTROY_WITH_I_H = false});
				Room2.Add( "Benkica", new ItemData( ){ ACTION= ItemActionType.move,   START_POS_ANCHORS = "A13", SCALE = new Vector2(40f,40f),  END_POS_ANCHOR = "E3", END_SCALE_FACTOR = 0.701f , ITEM_HOLDER = "DoorWashingMachine_A", DESTROY_WITH_I_H = false});
				Room2.Add( "Sock1", new ItemData( ){ ACTION= ItemActionType.move,   START_POS_ANCHORS = "A14", SCALE = new Vector2(36f,36f),  END_POS_ANCHOR = "E4", END_SCALE_FACTOR = 0.701f , ITEM_HOLDER = "DoorWashingMachine_A", DESTROY_WITH_I_H = false});
				Room2.Add( "Sock2", new ItemData( ){ ACTION= ItemActionType.move,   START_POS_ANCHORS = "A15", SCALE = new Vector2(36f,36f) , END_POS_ANCHOR = "E5",  END_SCALE_FACTOR = 0.701f , ITEM_HOLDER = "DoorWashingMachine_A", DESTROY_WITH_I_H = false});


				Room2.Add( "Apple1", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E6", START_POS_ANCHORS = "A16;A17", SCALE = new Vector2(36,36) });
				Room2.Add( "Apple2", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E7", START_POS_ANCHORS = "A18;A19", SCALE = new Vector2(36,36) });
				Room2.Add( "ShampooPurple", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E8", START_POS_ANCHORS = "A20", SCALE = new Vector2(36,36)});
				Room2.Add( "Sponge", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E9", START_POS_ANCHORS = "A21;A24", SCALE = new Vector2(36,36) });
				Room2.Add( "SoapBotle", new ItemData( ){ ACTION= ItemActionType.move,   END_POS_ANCHOR = "E10", START_POS_ANCHORS = "A28", SCALE = new Vector2(36,36)});

				Room2.Add( "Towel_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E14", START_POS_ANCHORS = "A29", SCALE = new Vector2(36,36) , START_ROT =  0, END_ROT = 0.01f});
				Room2.Add( "HeartBottle_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E18", START_POS_ANCHORS = "A30", SCALE = new Vector2(42,42) , START_ROT =  0, END_ROT = 0.01f });
				Room2.Add( "ToothBrush_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E19", START_POS_ANCHORS = "A31", SCALE = new Vector2(36,36) , START_ROT =  0, END_ROT = 0.01f  }); 
				Room2.Add( "ToothPaste_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E20", START_POS_ANCHORS = "A32", SCALE = new Vector2(36,36) , START_ROT =  90, END_ROT = 0.01f});
 

			}
			
			ActiveRoom = Room2;
			//28
		}


		if(room == 3 )
		{
			//TODO varijacije
			if( Room3.Count ==0)
			{
				Room3.Add( "Cobweb*1", new ItemData( ){ ACTION= ItemActionType.clean, START_POS_ANCHORS = "A1", SCALE = new Vector2(36,36), START_ROT = 0, TOOL =  ToolType.duster});
				Room3.Add( "Cobweb*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A2", SCALE = new Vector2(36,36)  ,START_ROT = 0, TOOL = ToolType.duster});
				Room3.Add( "Dirt*1", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A3",SCALE = new Vector2(36,36) , START_ROT = 0, TOOL = ToolType.brush});
				Room3.Add( "Dirt*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A4",SCALE = new Vector2(36,36) , START_ROT = 0, TOOL = ToolType.brush});
				Room3.Add( "Stain*1", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A5",SCALE = new Vector2(36,36) , START_ROT = 0, TOOL = ToolType.sponge});
				Room3.Add( "Stain*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A6",SCALE = new Vector2(36,36) , START_ROT = 0, TOOL = ToolType.sponge}); 
				Room3.Add( "Stain*3", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A32",SCALE = new Vector2(36,36) , START_ROT = 0, TOOL = ToolType.sponge}); 

				Room3.Add( "AppleBad*1", new ItemData( ){ ACTION= ItemActionType.dispose, SCALE = new Vector2(42,42) ,   START_POS_ANCHORS = "A7"  });
				Room3.Add( "AppleBad*2", new ItemData( ){ ACTION= ItemActionType.dispose, SCALE = new Vector2(42,42),   START_POS_ANCHORS = "A8" });
				Room3.Add( "CrumpledPaper*1", new ItemData( ){ ACTION= ItemActionType.dispose, SCALE = new Vector2(42,42),   START_POS_ANCHORS = "A9" });
				Room3.Add( "CrumpledPaper*2", new ItemData( ){ ACTION= ItemActionType.dispose, SCALE = new Vector2(42,42),   START_POS_ANCHORS = "A10" });
				Room3.Add( "BrokenPlate", new ItemData( ){ ACTION= ItemActionType.dispose, SCALE = new Vector2(42,42),   START_POS_ANCHORS = "A11" });
				Room3.Add( "BananaBad", new ItemData( ){ ACTION= ItemActionType.dispose, SCALE = new Vector2(36,36),   START_POS_ANCHORS = "A12" });

				Room3.Add( "Cup", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E1", START_POS_ANCHORS = "A13", SCALE = new Vector2(36,36) , ITEM_HOLDER = "Door_A", DESTROY_WITH_I_H = true });
				Room3.Add( "Cactus*1", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E2", START_POS_ANCHORS = "A14", SCALE = new Vector2(36,36),  END_POS_ANCHOR2="E3", PREFAB2 = "Cactus" });
				Room3.Add( "Cactus*2", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E3", START_POS_ANCHORS = "A15", SCALE = new Vector2(36,36), END_POS_ANCHOR2="E2", PREFAB2 = "Cactus" });
				Room3.Add( "Vinegar", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E4", START_POS_ANCHORS = "A16;A17", SCALE = new Vector2(36,36)});	
				Room3.Add( "Bread", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E5", START_POS_ANCHORS = "A18", SCALE = new Vector2(42,42) , ITEM_HOLDER = "BreadBoxLid_A", DESTROY_WITH_I_H = true});
 
				Room3.Add( "Door_A", new ItemData( ){ ACTION= ItemActionType.changeSprite,  END_POS_ANCHOR = "E6", START_POS_ANCHORS = "A19" , SCALE = new Vector2(35,35)  , START_ROT = 90, END_ROT = 0.01f,  ITEMS_LIST = "Cup;Cup2_B;Plate_B"});
 
				Room3.Add( "BreadBoxLid_A", new ItemData( ){ ACTION= ItemActionType.changeSprite,  END_POS_ANCHOR = "E7", START_POS_ANCHORS = "A20", SCALE = new Vector2(35,35) ,   ITEMS_LIST = "Bread" });
 

				Room3.Add( "Cup2_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E8", START_POS_ANCHORS = "A21", SCALE = new Vector2(35,35) , START_ROT = 90, END_ROT = -1f , ITEM_HOLDER = "Door_A", DESTROY_WITH_I_H = true});
				Room3.Add( "Plate_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E9", START_POS_ANCHORS = "A22", SCALE = new Vector2(35,35), ITEM_HOLDER = "Door_A", END_SCALE_FACTOR = 1f ,DESTROY_WITH_I_H = true});
	
				Room3.Add( "Salt_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E10", START_POS_ANCHORS = "A23", SCALE = new Vector2(35,35)  });
				Room3.Add( "Heart_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E11", START_POS_ANCHORS = "A24", SCALE = new Vector2(35,35)});
				Room3.Add( "CuttingBoard_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E12", START_POS_ANCHORS = "A25", SCALE = new Vector2(35,35) });
				Room3.Add( "Fork_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E13", START_POS_ANCHORS = "A26;A27", SCALE = new Vector2(42,42) });
				Room3.Add( "Spoon_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E14", START_POS_ANCHORS = "A28", SCALE = new Vector2(42,42) , START_ROT = 25, END_ROT = 0.01f });
				Room3.Add( "Knife_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E15", START_POS_ANCHORS = "A29", SCALE = new Vector2(35,35) });

				Room3.Add( "Glowe_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E16", START_POS_ANCHORS = "A30", SCALE = new Vector2(35,35)  , START_ROT = -25, END_ROT = 0.01f });
				Room3.Add( "TeaTowel_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E17", START_POS_ANCHORS = "A31", SCALE = new Vector2(35,35) });

 

			}
			
			ActiveRoom = Room3;
				// 30
			 

		}

		
		if(room == 4 )
		{
			if( Room4.Count ==0)
			{
				Room4.Add( "Cobweb*1", new ItemData( ){ ACTION= ItemActionType.clean, START_POS_ANCHORS = "A1", START_ROT = -25, TOOL =  ToolType.duster});
				Room4.Add( "Cobweb*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A2", SCALE = new Vector2(38,38)  ,START_ROT = 0, TOOL = ToolType.duster});
				Room4.Add( "Stain*1", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A5",SCALE = new Vector2(45,45) , START_ROT = 0, TOOL = ToolType.sponge});
				Room4.Add( "Stain*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A6",SCALE = new Vector2(25,35) , START_ROT = 0, TOOL = ToolType.sponge});
				Room4.Add( "Dirt*1", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A3",SCALE = new Vector2(30,25) , START_ROT = 0, TOOL = ToolType.brush});
				Room4.Add( "Dirt*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A4",SCALE = new Vector2(30,25) , START_ROT = 0, TOOL = ToolType.brush});
			 
				Room4.Add( "Picture1", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A7", START_ROT = 0, END_ROT = 8 , SCALE = new Vector2(30,32) });
				Room4.Add( "Picture2", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A8", START_ROT = 0, END_ROT = -8  , SCALE = new Vector2(30,32)});
				Room4.Add( "Lamp1", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A9",  START_ROT = 0, END_ROT = -33 , SCALE = new Vector2(36,36) });
				Room4.Add( "Lamp3", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A10", START_ROT = 0, END_ROT = -20 , SCALE = new Vector2(36,36)});
				Room4.Add( "Chandelier", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A11", START_ROT = 5, END_ROT = -2 , SCALE = new Vector2(36,36)});
				Room4.Add( "Crown", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A12",  START_ROT = -15, END_ROT = 10  , SCALE = new Vector2(36,36)});
				Room4.Add( "VaseAndFlowers", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A13", START_ROT = 0, END_ROT = 18 , SCALE = new Vector2(30,30) });
 
				Room4.Add( "Blanket_A", new ItemData( ) { ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E1", START_POS_ANCHORS = "A14" ,  SCALE = new Vector2(36,36) });
				Room4.Add( "BookBlue_A", new ItemData( ) { ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E2", START_POS_ANCHORS = "A15" ,  SCALE = new Vector2(36,36) });
				Room4.Add( "BookPink_A", new ItemData( ) { ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E3", START_POS_ANCHORS = "A16;A37" ,  SCALE = new Vector2(36,36) });

				Room4.Add( "Doily_A", new ItemData( ) { ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E5", START_POS_ANCHORS = "A18" ,  SCALE = new Vector2(36,36) });
				Room4.Add( "Flowers_A", new ItemData( ) { ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E6", START_POS_ANCHORS = "A19;A39" ,  SCALE = new Vector2(36,36) });
				Room4.Add( "WheelScrew_A", new ItemData( ) { ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E7", START_POS_ANCHORS = "A20;A38" ,  SCALE = new Vector2(36,36) });
				Room4.Add( "Cushion_A", new ItemData( ) { ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E8", START_POS_ANCHORS = "A21" ,  SCALE = new Vector2(35,35) });

				
				Room4.Add( "Curtain_A", new ItemData( ) { ACTION= ItemActionType.changeSprite,  END_POS_ANCHOR = "E4", START_POS_ANCHORS = "A17"  });

				Room4.Add( "Cactus*1", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E9", START_POS_ANCHORS = "A22",  END_POS_ANCHOR2="E10", PREFAB2 = "Cactus"	});
				Room4.Add( "Cactus*2", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E10", START_POS_ANCHORS = "A23" ,  END_POS_ANCHOR2="E9", PREFAB2 = "Cactus"	});
				Room4.Add( "CandleBlue", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E11", START_POS_ANCHORS = "A24",  SCALE = new Vector2(36,36) });
				Room4.Add( "CandleWhite", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E12", START_POS_ANCHORS = "A25",  SCALE = new Vector2(36,36) });
				Room4.Add( "TeddyBear", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E13", START_POS_ANCHORS = "A26",  SCALE = new Vector2(36,36) });
				Room4.Add( "CoffeeCup*1", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E14", START_POS_ANCHORS = "A27;A40" ,  END_POS_ANCHOR2="E15", PREFAB2 = "CoffeeCup"	});
				Room4.Add( "CoffeeCup*2", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E15", START_POS_ANCHORS = "A28;A36" ,  END_POS_ANCHOR2="E14", PREFAB2 = "CoffeeCup"	});
				Room4.Add( "Kitty", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E16", START_POS_ANCHORS = "A29",  SCALE = new Vector2(36,36) });

 

				Room4.Add( "AppleBad*1", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A30" });
				Room4.Add( "AppleBad*2", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A31" });

				Room4.Add( "Paper", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A32" ,  SCALE = new Vector2(36,36) });
				Room4.Add( "CrumpledPaper1*1", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A33",  SCALE = new Vector2(36,36) });
				Room4.Add( "CrumpledPaper1*2", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A34",  SCALE = new Vector2(36,36) });
			
				Room4.Add( "BananaBad", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A35",  SCALE = new Vector2(36,36) });
				
				
 
			}
			
			ActiveRoom = Room4;
			//33
		}




		if(room == 5 )
		{
			if( Room5.Count ==0)
			{
				Room5.Add( "Cobweb*1", new ItemData( ){ ACTION= ItemActionType.clean, START_POS_ANCHORS = "A1",SCALE = new Vector2(35,35) , START_ROT = 85, TOOL =  ToolType.duster});
				Room5.Add( "Cobweb*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A2",SCALE = new Vector2(35,35)  ,START_ROT = 20, TOOL = ToolType.duster});
				Room5.Add( "Cobweb*3", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A3;A4",SCALE = new Vector2(35,35) , START_ROT = 65, TOOL = ToolType.duster});
				Room5.Add( "Stain*1", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A13;A14",SCALE = new Vector2(35,35) , START_ROT = 0, TOOL = ToolType.sponge});
				Room5.Add( "Stain*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A15",SCALE = new Vector2(35,35) , START_ROT = 32, TOOL = ToolType.sponge});
				Room5.Add( "Stain*3", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A16;A17",SCALE = new Vector2(30,30) , START_ROT = 40, TOOL = ToolType.sponge});
				Room5.Add( "Dirt*1", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A44;A45",SCALE = new Vector2(30,30) , START_ROT = 0, TOOL = ToolType.brush});
				Room5.Add( "Dirt*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A46",SCALE = new Vector2(30,30) , START_ROT = 0, TOOL = ToolType.brush});
				Room5.Add( "Dirt*3", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A47;A48",SCALE = new Vector2(30,30) , START_ROT = 0, TOOL = ToolType.brush});

				 
				Room5.Add( "Scissors_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E1", START_POS_ANCHORS = "A5" , START_ROT = 0, END_ROT = 91});
				Room5.Add( "Pen_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E2", START_POS_ANCHORS = "A6;A7" , START_ROT = 0, END_ROT = 0.01f});
				Room5.Add( "Pencil_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E3", START_POS_ANCHORS = "A8;A9" , START_ROT = 0, END_ROT = 85});
				Room5.Add( "PinkBook_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E9", START_POS_ANCHORS = "A22" , START_ROT = 90, END_ROT = 0.01f});
				Room5.Add( "GreenBook_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E13", SCALE = new Vector2(55,55), END_SCALE_FACTOR = .7f  , START_POS_ANCHORS = "A39" , START_ROT = -90, END_ROT = 0.01f});
				Room5.Add( "BlueBook_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E14", START_POS_ANCHORS = "A41" ,END_SCALE_FACTOR = .8f  , START_ROT = -90, END_ROT = 0.01f});
				Room5.Add( "BrownBook_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E15", START_POS_ANCHORS = "A42" ,END_SCALE_FACTOR = 1.2f  ,  START_ROT = 0, END_ROT = 0.01f});
				 

				Room5.Add( "LampSpring_A", new ItemData( ){ ACTION= ItemActionType.changeSprite,  END_POS_ANCHOR = "E4", START_POS_ANCHORS = "A10", START_ROT = 0, END_ROT = 42});
				Room5.Add( "Vase_A", new ItemData( ){ ACTION= ItemActionType.changeSprite,  END_POS_ANCHOR = "E6", START_POS_ANCHORS = "A12" , START_ROT = 90, END_ROT = 0.01f  });

				Room5.Add( "Box", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E5", START_POS_ANCHORS = "A11"});
				Room5.Add( "Quill", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E7", START_POS_ANCHORS = "A18;A19"});
				Room5.Add( "PinkCandle", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E8", START_POS_ANCHORS = "A20;A21"});
				Room5.Add( "PalmTree", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E10", START_POS_ANCHORS = "A24"});
				Room5.Add( "BlueCandle", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E11", START_POS_ANCHORS = "A27;A28"});
				Room5.Add( "CoffeeCup", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E12", START_POS_ANCHORS = "A33;A34"});


				Room5.Add( "Paper", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A23"  });
				Room5.Add( "BrokenPlate", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A29" });
				Room5.Add( "CrumpledPaper1*1", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A30" });
				Room5.Add( "CrumpledPaper1*2", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A31" });
				Room5.Add( "Apple", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A35;A36" });
				Room5.Add( "Banana", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A37" });


				Room5.Add( "Painting3", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A25", START_ROT = 0, END_ROT = -8 });
				Room5.Add( "Painting2", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A26", START_ROT = 0, END_ROT = 6 });
				Room5.Add( "Chandelier", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A38",  START_ROT = 8, END_ROT = -2 });
				Room5.Add( "Clock", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A43", START_ROT = 0, END_ROT = -5 , SCALE = new Vector2(53,53) });

				Room5.Add( "Curtain", new ItemData( ){ ACTION= ItemActionType.eraseSprite,   START_POS_ANCHORS = "A40",SCALE = new Vector2(110,110) });
			}

			ActiveRoom = Room5;
			//35
		}

		if(room == 6 )
		{
			if( Room6.Count ==0)
			{
				Room6.Add( "Cobweb*1", new ItemData( ){ ACTION= ItemActionType.clean, START_POS_ANCHORS = "A1;A2", SCALE = new Vector2(35,35), START_ROT =  0, TOOL =  ToolType.duster});
				Room6.Add( "Cobweb*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A3", SCALE = new Vector2(35,35)  ,START_ROT = 0, TOOL = ToolType.duster});
				Room6.Add( "Cobweb*3", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A4", SCALE = new Vector2(35,35)  ,START_ROT = 0, TOOL = ToolType.duster});
				Room6.Add( "Stain*1", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A5",SCALE = new Vector2(35,35) , START_ROT = 0, TOOL = ToolType.sponge});
				Room6.Add( "Stain*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A6;A7",SCALE = new Vector2(35,35) , START_ROT = 0, TOOL = ToolType.sponge}); 
				Room6.Add( "Dirt*1", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A8",SCALE = new Vector2(30,25) , START_ROT = 0, TOOL = ToolType.brush});
				Room6.Add( "Dirt*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A9",SCALE = new Vector2(30,25) , START_ROT = 0, TOOL = ToolType.brush});


				Room6.Add( "Apple*1", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A10", SCALE = new Vector2(42, 42)  });
				Room6.Add( "Apple*2", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A11", SCALE = new Vector2(42, 42)  });
				Room6.Add( "Banana", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A12", SCALE = new Vector2(35, 35)  });
				Room6.Add( "CrumpledPaper*1", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A13", SCALE = new Vector2(42, 42) });
				Room6.Add( "CrumpledPaper*2", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A14", SCALE = new Vector2(42, 42) });
				Room6.Add( "BrokenPlate", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A15", SCALE = new Vector2(35, 35)   });

				Room6.Add( "Poster", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A16" , SCALE = new Vector2(35,35), START_ROT = -13, END_ROT =  0.01f  });
				Room6.Add( "MushrumShade", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A17" , SCALE = new Vector2(35,35), START_ROT = -20, END_ROT =  0.01f  });
				Room6.Add( "O", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A18", SCALE = new Vector2(35,35), START_ROT = 70, END_ROT = 0.01f });
				Room6.Add( "Note", new ItemData( ){ ACTION= ItemActionType.rotate ,   START_POS_ANCHORS = "A19" , SCALE = new Vector2(35,35), START_ROT = 15, END_ROT = 0.01f });
				Room6.Add( "Painting", new ItemData( ){ ACTION= ItemActionType.rotate,  START_POS_ANCHORS = "A20" , SCALE = new Vector2(35,35), START_ROT =  13, END_ROT =  0.01f });
 

				Room6.Add( "TeaPot", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E1", START_POS_ANCHORS = "A21;A22", SCALE = new Vector2(35,35) ,START_ROT = 90, END_ROT = 90f  });
				Room6.Add( "LavaLamp", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E2", START_POS_ANCHORS = "A23", SCALE = new Vector2(30,30) });
				Room6.Add( "FlowerPot1", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E3", START_POS_ANCHORS = "A24;A25", SCALE = new Vector2(35,35) ,START_ROT = -90, END_ROT = -90f ,  END_POS_ANCHOR2="E4", PREFAB2 = "FlowerPot2"});
				Room6.Add( "FlowerPot2", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E4", START_POS_ANCHORS = "A26;A27" , SCALE = new Vector2(35,35),  START_ROT = -90, END_ROT = -90f , END_POS_ANCHOR2="E3", PREFAB2 = "FlowerPot1"});

				Room6.Add( "Lego1", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E5", START_POS_ANCHORS = "A28" , SCALE = new Vector2(35,35),  END_POS_ANCHOR2="E6", PREFAB2 = "Lego2"});
				Room6.Add( "Lego2", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E6", START_POS_ANCHORS = "A29;A30" , SCALE = new Vector2(35,35),  END_POS_ANCHOR2="E5", PREFAB2 = "Lego1"});

				Room6.Add( "Puppy", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E7", START_POS_ANCHORS = "A31", SCALE = new Vector2(35,35) , START_ROT =  90,END_ROT =  90f });
				Room6.Add( "Kitty", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E8", START_POS_ANCHORS = "A32", SCALE = new Vector2(35,35) });
				Room6.Add( "TeddyBear", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E9", START_POS_ANCHORS = "A33", SCALE = new Vector2(35,35) });

 
 

				Room6.Add( "TeaCup1", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E11", SCALE = new Vector2(35,35), START_POS_ANCHORS = "A36;A37",  END_POS_ANCHOR2="E12", PREFAB2 = "TeaCup2"});
				Room6.Add( "TeaCup2", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E12", SCALE = new Vector2(35,35), START_POS_ANCHORS = "A38;A39",  END_POS_ANCHOR2="E11", PREFAB2 = "TeaCup1"});



				Room6.Add( "LetterBricks", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E10", START_POS_ANCHORS = "A34;A35", SCALE = new Vector2(45,45) ,	END_SCALE_FACTOR = 0.001f, ITEM_HOLDER = "ToyChestLid_A", DESTROY_WITH_I_H = true });
				Room6.Add( "Ball", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E13", START_POS_ANCHORS = "A40", SCALE = new Vector2(35,35), END_SCALE_FACTOR = 0.001f, ITEM_HOLDER = "ToyChestLid_A", DESTROY_WITH_I_H = true });
				Room6.Add( "RubberDucky", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E14", START_POS_ANCHORS = "A41", SCALE = new Vector2(35,35) , 	END_SCALE_FACTOR = 0.001f, ITEM_HOLDER = "ToyChestLid_A", DESTROY_WITH_I_H = true });
				Room6.Add( "Airplane", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E15", START_POS_ANCHORS = "A42", SCALE = new Vector2(35, 35),	END_SCALE_FACTOR = 0.001f, ITEM_HOLDER = "ToyChestLid_A", DESTROY_WITH_I_H = true });
				Room6.Add( "Car", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E16", START_POS_ANCHORS = "A43", SCALE = new Vector2(35,35),	END_SCALE_FACTOR = 0.001f, ITEM_HOLDER = "ToyChestLid_A", DESTROY_WITH_I_H = true });
				Room6.Add( "Elephant", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E17", START_POS_ANCHORS = "A44", SCALE = new Vector2(25, 25),	END_SCALE_FACTOR = 0.001f, ITEM_HOLDER = "ToyChestLid_A", DESTROY_WITH_I_H = true });
				Room6.Add( "Octopuss", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E18", START_POS_ANCHORS = "A45", SCALE = new Vector2(25,25), END_SCALE_FACTOR = 0.001f, ITEM_HOLDER = "ToyChestLid_A", DESTROY_WITH_I_H = true });

				Room6.Add( "ToyChestLid_A", new ItemData( ){ ACTION= ItemActionType.changeSprite,  END_POS_ANCHOR = "E19", START_POS_ANCHORS = "A46" ,  SCALE = new Vector2(35,35), START_ROT = 0, END_SCALE_FACTOR = 1f, END_ROT =0.01f,
					ITEMS_LIST = "Ball;RubberDucky;Airplane;LetterBricks;Car;Elephant;Octopuss"});


				Room6.Add( "Book_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E20", SCALE = new Vector2(35,35), START_POS_ANCHORS = "A47", START_ROT = 0, END_ROT = 0});
				Room6.Add( "Book2_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E21", SCALE = new Vector2(35,35), START_POS_ANCHORS = "A48" , START_ROT = 0, END_ROT = 90f});
 
			}
			
			ActiveRoom = Room6;
		}

		if(room == 7 )
		{
			if( Room7.Count ==0)
			{
				Room7.Add( "Cobweb*1", new ItemData( ){ ACTION= ItemActionType.clean, START_POS_ANCHORS = "A1", SCALE = new Vector2(35,35), START_ROT =  0, TOOL =  ToolType.duster});
				Room7.Add( "Cobweb*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A2", SCALE = new Vector2(35,35)  ,START_ROT = 65, TOOL = ToolType.duster});
				Room7.Add( "Cobweb*3", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A3", SCALE = new Vector2(35,35)  ,START_ROT = 0, TOOL = ToolType.duster});
				Room7.Add( "Stain*1", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A4",SCALE = new Vector2(35,35) , START_ROT = 0, TOOL = ToolType.sponge});
				Room7.Add( "Stain*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A5",SCALE = new Vector2(35,35) , START_ROT = 0, TOOL = ToolType.sponge}); 
				Room7.Add( "Stain*3", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A6",SCALE = new Vector2(35,35) , START_ROT = 148, TOOL = ToolType.sponge}); 
				Room7.Add( "Dirt*1", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A7",SCALE = new Vector2(35,35) , START_ROT = 0, TOOL = ToolType.brush});
				Room7.Add( "Dirt*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A8",SCALE = new Vector2(35,35) , START_ROT = 0, TOOL = ToolType.brush});
				Room7.Add( "Dirt*3", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A9",SCALE = new Vector2(35,35) , START_ROT = 0, TOOL = ToolType.brush});

				Room7.Add( "Drawer", new ItemData( ){ ACTION= ItemActionType.eraseSprite,   START_POS_ANCHORS = "A10", SCALE = new Vector2(35,35) , ITEMS_LIST = "Shorts;Sock1;Sock2;Sock3;Sock4"});

				Room7.Add( "Picture1", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A11" , SCALE = new Vector2(35,35), START_ROT = 15, END_ROT =  0.01f  });
				Room7.Add( "Picture2", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A12" , SCALE = new Vector2(35,35), START_ROT = -10, END_ROT =  0.01f  });
				Room7.Add( "Picture3", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A13", SCALE = new Vector2(35,35), START_ROT = 20, END_ROT = 0.01f });



 
				Room7.Add( "BrokenPlate", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A14" , SCALE = new Vector2(35,35)   });
				Room7.Add( "CrumpledPaper*1", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A15", SCALE = new Vector2(40,40) });
				Room7.Add( "CrumpledPaper*2", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A16", SCALE = new Vector2(40, 40) });
				Room7.Add( "CrumpledPaper*3", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A17", SCALE = new Vector2(40, 40) });
				Room7.Add( "AppleBad*1", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A18" , SCALE = new Vector2(35,35)});
				Room7.Add( "AppleBad*2", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A19" , SCALE = new Vector2(35,35)});
				Room7.Add( "BananaBad", new ItemData( ){ ACTION= ItemActionType.dispose,   START_POS_ANCHORS = "A20" , SCALE = new Vector2(35,35) });

				Room7.Add( "Shoes1", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E1", START_POS_ANCHORS = "A21", SCALE = new Vector2(35,35) });
				Room7.Add( "Shoe2", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E2", START_POS_ANCHORS = "A22;A23", SCALE = new Vector2(35,35) });
				Room7.Add( "Shoe3", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E3", START_POS_ANCHORS = "A24;A25", SCALE = new Vector2(35,35) });
				Room7.Add( "Shoe4", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E4", START_POS_ANCHORS = "A26;A27", SCALE = new Vector2(35,35) });
				Room7.Add( "Shoe5", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E9", START_POS_ANCHORS = "A33", SCALE = new Vector2(35,35) });
 

				Room7.Add( "Stool_A", new ItemData( ){ ACTION= ItemActionType.changeSprite,  END_POS_ANCHOR = "E5", START_POS_ANCHORS = "A28", SCALE = new Vector2(35,35), START_ROT = 0,  END_ROT =-90f  });
				Room7.Add( "Vase_A", new ItemData( ){ ACTION= ItemActionType.changeSprite,  END_POS_ANCHOR = "E6", START_POS_ANCHORS = "A29", SCALE = new Vector2(35,35), START_ROT = 90,  END_ROT = 0.01f  });
				Room7.Add( "Knob_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E7", START_POS_ANCHORS = "A30;A31", SCALE = new Vector2(35,35)  });

				Room7.Add( "Vest_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E8", START_POS_ANCHORS = "A32", SCALE = new Vector2(35,35)  });
				Room7.Add( "Sweater_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E10", START_POS_ANCHORS = "A34", SCALE = new Vector2(35,35)  });
				Room7.Add( "Benka_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E11", START_POS_ANCHORS = "A35", SCALE = new Vector2(35,35)  });
				Room7.Add( "Jacket_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E12", START_POS_ANCHORS = "A36", SCALE = new Vector2(35,35)  });
				Room7.Add( "Shirt_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E13", START_POS_ANCHORS = "A37", SCALE = new Vector2(35,35), START_ROT = 0,  END_ROT =90f    });


 
 
				Room7.Add( "Shorts", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E14",  START_POS_ANCHORS = "A38" , END_SCALE_FACTOR = 0.001f , SCALE = new Vector2(35,35) , ITEM_HOLDER = "Drawer", DESTROY_WITH_I_H = true });
				Room7.Add( "Sock1", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E15", START_POS_ANCHORS = "A39" , END_SCALE_FACTOR = 0.001f, SCALE = new Vector2(35,35), ITEM_HOLDER = "Drawer", DESTROY_WITH_I_H = true });
				Room7.Add( "Sock2", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E16",  START_POS_ANCHORS = "A40", END_SCALE_FACTOR = 0.001f, SCALE = new Vector2(35,35) , ITEM_HOLDER = "Drawer", DESTROY_WITH_I_H = true });
				Room7.Add( "Sock3", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E17",  START_POS_ANCHORS = "A41", END_SCALE_FACTOR = 0.001f,  SCALE = new Vector2(35,35) ,   ITEM_HOLDER = "Drawer", DESTROY_WITH_I_H = true  });
				Room7.Add( "Sock4", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E18",  START_POS_ANCHORS = "A42", END_SCALE_FACTOR = 0.001f, SCALE = new Vector2(35,35) ,  ITEM_HOLDER = "Drawer", DESTROY_WITH_I_H = true });


			

			 

			}
			
			ActiveRoom = Room7;
		}



		if(room == 8 )
		{
			//TODO varijacije
			if( Room8.Count ==0)
			{
				Room8.Add( "Cobweb*1", new ItemData( ){ ACTION= ItemActionType.clean, START_POS_ANCHORS = "A1", SCALE = new Vector2(35,35), START_ROT = 0, TOOL = ToolType.duster});
				Room8.Add( "Cobweb*2", new ItemData( ){ ACTION= ItemActionType.clean, START_POS_ANCHORS = "A2", SCALE = new Vector2(35,35), START_ROT = 0, TOOL = ToolType.duster});
				Room8.Add( "Cobweb*3", new ItemData( ){ ACTION= ItemActionType.clean, START_POS_ANCHORS = "A3", SCALE = new Vector2(35,35), START_ROT = 0, TOOL = ToolType.duster});

				Room8.Add( "Stain*1", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A4",SCALE = new Vector2(45,45) , START_ROT = 0, TOOL = ToolType.sponge});
				Room8.Add( "Stain*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A5",SCALE = new Vector2(45,45) , START_ROT = 0, TOOL = ToolType.sponge}); 
				Room8.Add( "Stain*3", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A6",SCALE = new Vector2(30,40) , START_ROT = 0, TOOL = ToolType.sponge}); 

				Room8.Add( "Dirt*1", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A7",SCALE = new Vector2(30,25) , START_ROT = 0, TOOL = ToolType.brush});
				Room8.Add( "Dirt*2", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A8",SCALE = new Vector2(30,25) , START_ROT = 0, TOOL = ToolType.brush});
				Room8.Add( "Dirt*3", new ItemData( ){ ACTION= ItemActionType.clean,   START_POS_ANCHORS = "A9",SCALE = new Vector2(30,25) , START_ROT = 0, TOOL = ToolType.brush});
				
				Room8.Add( "Drawer", new ItemData( ){ ACTION= ItemActionType.eraseSprite,   START_POS_ANCHORS = "A10",SCALE = new Vector2(36,36) , ITEMS_LIST = "Bracelete;Necklace;Tiara"});

				Room8.Add( "Door_A", new ItemData( ){ ACTION= ItemActionType.changeSprite,  END_POS_ANCHOR = "E1", SCALE = new Vector2(34,35), START_POS_ANCHORS = "A11" , START_ROT = 0, END_ROT = 0.01f ,
					ITEMS_LIST = "Comb;Wig2;Wig1;HairStraightener;HairCurlers;HairDryer;HairBrushFlat;HairBrushRound;Powder"});
				Room8.Add( "Perfume_A", new ItemData( ){ ACTION= ItemActionType.changeSprite,  END_POS_ANCHOR = "E2", SCALE = new Vector2(55,55), START_POS_ANCHORS = "A12" , START_ROT = 0, END_ROT = 0.01f});
				Room8.Add( "Poster_A", new ItemData( ){ ACTION= ItemActionType.changeSprite,  END_POS_ANCHOR = "E3", SCALE = new Vector2(21,22), START_POS_ANCHORS = "A13" , START_ROT = 0, END_ROT =0.01f});


				Room8.Add( "GreenBook_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E4", START_POS_ANCHORS = "A14", SCALE = new Vector2(51,51),  END_SCALE_FACTOR = 0.7f });
				Room8.Add( "PinkBook_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E5", START_POS_ANCHORS = "A15", SCALE = new Vector2(51,51) , END_SCALE_FACTOR = 0.7f });
				Room8.Add( "Shoe_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E6", START_POS_ANCHORS = "A16", SCALE = new Vector2(36,36) , START_ROT = -90, END_ROT = 0.01f});
				Room8.Add( "Brush_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E7", START_POS_ANCHORS = "A17", SCALE = new Vector2(33,33), START_ROT = 0, END_ROT = 0.01f});
				Room8.Add( "Flower_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E8", START_POS_ANCHORS = "A18", SCALE = new Vector2(30,30), START_ROT = 0, END_ROT = 0.01f});
				Room8.Add( "EyeShadow_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E9", START_POS_ANCHORS = "A19", SCALE = new Vector2(30,30), START_ROT = 0, END_ROT = 0.01f});
				Room8.Add( "Brush2_A", new ItemData( ){ ACTION= ItemActionType.moveAndChangeSprite,  END_POS_ANCHOR = "E10", START_POS_ANCHORS = "A20", SCALE = new Vector2(33,33), START_ROT = 0, END_ROT = 0.01f});


				Room8.Add( "Chandelier", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A21", SCALE = new Vector2(40, 40),START_ROT = 5, END_ROT = -2 });
				Room8.Add( "Lamp", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A22", SCALE = new Vector2(30, 30), START_ROT = 0, END_ROT = 5 });
				Room8.Add( "Picture1", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A23" , SCALE = new Vector2(25, 25) , START_ROT = 0, END_ROT =  -9  });
				Room8.Add( "Picture2", new ItemData( ){ ACTION= ItemActionType.rotate,   START_POS_ANCHORS = "A24", SCALE = new Vector2(30, 30), START_ROT = -18, END_ROT = -3 });

			
				Room8.Add( "CandlePink", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E11", START_POS_ANCHORS = "A25", SCALE = new Vector2(36,36)  });
				Room8.Add( "CandleWhite", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E12", START_POS_ANCHORS = "A26", SCALE = new Vector2(36,36)  });
				Room8.Add( "Powder", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E13", START_POS_ANCHORS = "A27", SCALE = new Vector2(32,32), END_SCALE_FACTOR = 0.8f   , ITEM_HOLDER = "Door_A" , DESTROY_WITH_I_H = true});
				Room8.Add( "Perfume2", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E14", START_POS_ANCHORS = "A28", SCALE = new Vector2(36,36)  });
				Room8.Add( "HairBrushRound", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E15", START_POS_ANCHORS = "A29", SCALE = new Vector2(36,36), END_SCALE_FACTOR = 0.7f   , ITEM_HOLDER = "Door_A" , DESTROY_WITH_I_H = true});
				Room8.Add( "HairBrushFlat", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E16", START_POS_ANCHORS = "A30", SCALE = new Vector2(36,36), END_SCALE_FACTOR = 0.7f, START_ROT = 90   , ITEM_HOLDER = "Door_A" , DESTROY_WITH_I_H = true});
				Room8.Add( "HairDryer", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E17", START_POS_ANCHORS = "A31", SCALE = new Vector2(28,28), END_SCALE_FACTOR = 0.6f  , ITEM_HOLDER = "Door_A" , DESTROY_WITH_I_H = true });
				Room8.Add( "HairCurlers", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E18", START_POS_ANCHORS = "A32", SCALE = new Vector2(36,36), END_SCALE_FACTOR = 0.7f   , ITEM_HOLDER = "Door_A" , DESTROY_WITH_I_H = true});
				Room8.Add( "HairStraightener", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E19", START_POS_ANCHORS = "A33", SCALE = new Vector2(30,30), END_SCALE_FACTOR = 0.7f   , ITEM_HOLDER = "Door_A" , DESTROY_WITH_I_H = true});
				Room8.Add( "Wig1", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E20", START_POS_ANCHORS = "A34", SCALE = new Vector2(30,30), END_SCALE_FACTOR = 0.8f   , ITEM_HOLDER = "Door_A" , DESTROY_WITH_I_H = true});
				Room8.Add( "Wig2", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E21", START_POS_ANCHORS = "A35", SCALE = new Vector2(30,30), END_SCALE_FACTOR = 0.8f   , ITEM_HOLDER = "Door_A" , DESTROY_WITH_I_H = true});
				Room8.Add( "Comb", new ItemData( ){ ACTION= ItemActionType.move,  END_POS_ANCHOR = "E22", START_POS_ANCHORS = "A36", SCALE = new Vector2(33,33), END_SCALE_FACTOR = 0.8f  , ITEM_HOLDER = "Door_A" , DESTROY_WITH_I_H = true });
 
				//fioka
				Room8.Add( "Bracelete", new ItemData( ){ ACTION= ItemActionType.move,   START_POS_ANCHORS = "A37", SCALE = new Vector2(33,33) , END_POS_ANCHOR = "E23", END_SCALE_FACTOR = 0.45f,  ITEM_HOLDER = "Drawer" , DESTROY_WITH_I_H = true  });
				Room8.Add( "Necklace", new ItemData( ){ ACTION= ItemActionType.move,   START_POS_ANCHORS = "A38", SCALE = new Vector2(33,33) ,END_POS_ANCHOR = "E24", END_SCALE_FACTOR = 0.45f , ITEM_HOLDER = "Drawer", DESTROY_WITH_I_H = true });
				Room8.Add( "Tiara", new ItemData( ){ ACTION= ItemActionType.move,   START_POS_ANCHORS = "A39" , SCALE = new Vector2(33,33) ,END_POS_ANCHOR = "E25", END_SCALE_FACTOR = 0.45f , ITEM_HOLDER = "Drawer", DESTROY_WITH_I_H = true });

			}
			
			ActiveRoom = Room8;
		}

		


	}

}

public class ItemData
{
	public ItemActionType ACTION;
	public Vector2 SCALE =45* Vector2.one;
	public float END_SCALE_FACTOR =1;
	public string END_POS_ANCHOR = "";
	public string START_POS_ANCHORS = "";
	public int START_ROT = 0;
	public float END_ROT = -999; //ako ostane -999 onda se prepisuje pocetna rotacija START_ROT
	public ToolType TOOL = ToolType.none;

	public string ITEM_HOLDER = "";
	public bool DESTROY_WITH_I_H = false;
	public string ITEMS_LIST = "";

	//mogu i  vise alternativnih, razdvojenih sa ';'
	public string END_POS_ANCHOR2 = "";
	public string PREFAB2 = "";
}
